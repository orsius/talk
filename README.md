# Rennes mai 2023

**Create a Blank Project**:

- [ ] Project Name: demo
- [ ] Visibility Level: public

**Project Configuration**:
- [ ] Initialize Repository with a README: true (:ballot_box_with_check:)

Afterwards:

**Settings > Repository**
- [ ] Protected Branches:

| Branch  | Allowed to Merge | Allowed to Push and Merge | Allowed to Force Push |
| ------- | ---------------- | ------------------------- | --------------------- |
| main    | Maintainers      | No one                    | ❌                    |

**Settings > Merge Requests**		
- [ ] Merge Checks: All Threads Must Be Resolved => true (:ballot_box_with_check:)

**Issues > Import issues**		
- [ ] import from csv file here

**Issues > Milestones**		
- [ ] Create a milestone like this :  
Title : Getting Things Done  
Description :  
````md
![devsecops_lifecycle.svg](https://gitlab.com/neosoft-group/practice/devops/public/talk-it/rennes-mai-2023/talk/uploads/850a16bfc5f2662ce6b37e21caed169a/devsecops_lifecycle.svg)
````

**Issues > Bulk edit**		
- [ ] select all issues and set milestone to "Getting Things Done"